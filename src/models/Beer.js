export class Beer {
    name;
    description;
    id;
    image_url;

    constructor (name, description, id, image_url){
        this.name = name
        this.description = description
        this.id = id
        this.image_url = image_url
    }
}