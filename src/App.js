import React from 'react';
import { Beer } from './components/Beer';
import './App.scss';

function App() {
  return (
    <div className="App">
      <Beer /> 
    </div>
  );
}

export default App;
