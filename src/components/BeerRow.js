//function Beer(){}
import React, { useState } from 'react'
import '../App.scss'

export const BeerRow = ({label, value}) => {

    const [isValueCrossed, setIsValueCrossed] = useState(false)

    const _onLabelClick = () => {
        setIsValueCrossed(true)
    }

    return (
        <div className='BeerRow'>
            <div className='Label' onClick={_onLabelClick}>{label}&nbsp;</div>
            <div className={isValueCrossed && 'ValueCrossed'}>{value}</div>
        </div>
        )
}