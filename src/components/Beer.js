//function Beer(){}
import React, { useEffect, useState } from 'react'
import { getBeer } from '../services/BeerService';
import { BeerRow } from './BeerRow';


export const Beer = ()=>{

    const [beer, setBeer] = useState(null)

    const _getBeer = async () => {
       const _beer = await getBeer()
       setBeer(_beer)
    }

    useEffect(() => {
        _getBeer()
    },[])

    console.log(beer?.image_url)
    return beer ? (
        <div className='Beer'>
            <img src={beer.image_url} alt="" />
            <div>
                <BeerRow label="Nom:" value={beer.name}/>
                <BeerRow label="Description:" value={beer.description}/>
                <BeerRow label="ID:" value={beer.id}/>
            </div>     
        </div>
        ) : null
}