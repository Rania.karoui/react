import axios from 'axios'
import { Beer } from '../models/Beer';

// const getBeer = () => {
//     axios.get('https://randomuser.me/')
//     .then((response) => {
//         console.log(response)
//     })
//     .catch((e) => {
//         console.log(e)
//     })
// }

export const getBeer = async () => {
    try {
        const response = await axios.get('https://api.punkapi.com/v2/beers/random')
        const beer = response.data[0]
        console.log(response)
        const { name, description, id, image_url} = beer
        return new Beer(name, description, id, image_url)
    } catch (e) {
        console.log(e)
    }
}